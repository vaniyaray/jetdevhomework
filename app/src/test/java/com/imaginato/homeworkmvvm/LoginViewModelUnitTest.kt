package com.imaginato.homeworkmvvm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.imaginato.homeworkmvvm.data.remote.login.LoginApi
import com.imaginato.homeworkmvvm.data.remote.login.UserDataRepository
import com.imaginato.homeworkmvvm.data.remote.login.request.LoginRequest
import com.imaginato.homeworkmvvm.data.remote.login.response.ApiResponse
import com.imaginato.homeworkmvvm.data.remote.login.response.LoginResponse
import com.imaginato.homeworkmvvm.domain.*
import com.imaginato.homeworkmvvm.ui.login.LoginViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinApiExtension
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException


@OptIn(KoinApiExtension::class)
class LoginViewModelUnitTest {

    private lateinit var viewModel: LoginViewModel
    private var loginApi: LoginApi = mock {}
    private lateinit var repository: UserDataRepository
    private val dispatcher = StandardTestDispatcher()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setInit() {
        Dispatchers.setMain(dispatcher)
        repository = UserDataRepository(loginApi)
        viewModel = LoginViewModel()
        startKoin {
            androidLogger(Level.ERROR)
            modules(
                databaseModule,
                netModules,
                apiModules,
                repositoryModules,
                viewModelModules,
                loginApiModules
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun usernameIsEmpty() {
        viewModel.isDataValid("", "")
        assert(viewModel.uiMessage.awaitValue() == R.string.please_enter_username)
    }

    @Test
    fun userNameIsNotEmpty() {
        viewModel.isDataValid("abc", "")
        assert(viewModel.uiMessage.awaitValue() != R.string.please_enter_username)
    }

    @Test
    fun passwordIsEmpty() {
        viewModel.isDataValid("abc", "")
        assert(viewModel.uiMessage.awaitValue() == R.string.please_enter_password)
    }

    @Test
    fun passwordIsNotEmpty() {
        viewModel.isDataValid("abcd", "abcd")
        assert(viewModel.uiMessage.awaitValue() != R.string.please_enter_password)
    }

    @Test
    fun loginApiCall() = runTest(dispatcher) {

        val url = "https://private-222d3-homework5.apiary-mock.com/api/login"
        val request = LoginRequest("username", "1111111")
        whenever(
            loginApi.doLogin(
                url,
                request
            )
        ).doReturn(Response.success(ApiResponse(data = LoginResponse("83878737"))))
        viewModel.login(request)

        assert(viewModel.apiResponse.getOrAwaitValue() != null)
    }

    fun <T> LiveData<T>.getOrAwaitValue(
        time: Long = 10,
        timeUnit: TimeUnit = TimeUnit.SECONDS,
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(o: T?) {
                data = o
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }

}

fun <T> LiveData<T>.awaitValue(
    time: Long = 2,
    timeUnit: TimeUnit = TimeUnit.SECONDS
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data = o
            latch.countDown()
            this@awaitValue.removeObserver(this)
        }
    }

    this.observeForever(observer)

    if (!latch.await(time, timeUnit)) {
        throw TimeoutException("Timeout, Please try again.")
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}
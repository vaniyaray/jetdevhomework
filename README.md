# Login API integrated using Kotlin with MVVM


### What's in the box

- Login screen with username and password as input.

### How to use...

1. Make sure you have working internet connection.
2. Enter username (i.e username).
3. Enter password (i.e. 1111111).
4. Press login button to call login api.
5. After login is successful, user data will be saved in to the local database using RoomDB.


### User full links

- (AndroidX versions): https://developer.android.com/jetpack/androidx/versions

- (ViewBinding): https://proandroiddev.com/migrating-the-deprecated-kotlin-android-extensions-compiler-plugin-to-viewbinding-d234c691dec7


### References

- (Android-Clean-Architecture): https://github.com/android10/Android-CleanArchitecture-Kotlin

- (Koin): https://insert-koin.io


### Naming conversation style

- ImageView: ivLogo 
- EditText: etUserName, etPassword 
- Button: btnLogin